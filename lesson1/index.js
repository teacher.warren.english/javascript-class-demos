// here we can add javascript!
var myFirstVariable = "Hello World!" // Avoid this var variable!

let mySecondVar = "My Let Variable" // mutable (can be reassigned)
const myThirdVar = 'My Const Variable' // immutable (cannot be reassigned)

mySecondVar = "I've changed!"
//myThirdVar = "I've also changed!"

mySecondVar = mySecondVar + " I've been concatenated!"

let myAge = 30
// myAge = myAge + 1
// myAge += 1
// myAge -= 1
// myAge --

console.log(this);

// Abuse Window Functionality
// alert("Here's your first alert!");
// let favColor = prompt("What's your favorite color?")

// DOM - Document Object Model
const mySubHeading = document.getElementById("subheader") // " " = ' '
mySubHeading.innerText = myThirdVar;

const myParagraph = document.getElementById('paragraph-text')
myParagraph.innerText = "I am " + myAge + " years old."

const btnIncrease = document.getElementById("btn-increase-age")
const btnDecrease = document.getElementById("btn-decrease-age")

btnIncrease.addEventListener('click', btnIncreaseCLick)
btnDecrease.addEventListener('click', btnDecreaseCLick)

// Handlers
function btnIncreaseCLick(){
    increaseAge();
    console.log("btnIncreaseClick", myAge);
    console.log(this);
}

function btnDecreaseCLick(){
    decreaseAge();
    console.log("btnDecreaseClick", myAge);
}

// functions

function increaseAge() {
    myAge++
    myParagraph.innerText = "I am " + myAge + " years old."
}

function decreaseAge() {
    myAge--
    myParagraph.innerText = "I am " + myAge + " years old."
}

// Javascript Object (JSON)
const warren = {
    // props
    name: "Warren West",
    age: 30,
    favColor: "Red",
    // methods
    printName(){
        console.log("I am " + this.name);
    }
}

warren.printName()