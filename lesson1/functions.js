function createPizza(size, toppings){
    // small / medium / large
    const pizza = {
        size: size,
        toppings: toppings,
        price: size.length + toppings.length + " Krone"
    }
    // if we need a return keyword
    return pizza
}

// types: string, number, boolean, undefined, null, NaN
const myFavPizza = createPizza("large", "pineapple, vegan mince, mango")

console.log("My Pizza: " + myFavPizza.size + " " + myFavPizza.toppings + " " + myFavPizza.price);

//const myPizzaDisplay = document.getElementById("pizza-display")
