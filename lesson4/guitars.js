// const guitars = [] // this will disappear

// .then((jsonGuitars) => {
//     // render the guitars
//     guitars.push(...jsonGuitars) // spread!
//     renderGuitars(jsonGuitars)
// })

async function fetchGuitars() {
    try {
        const response = await fetch("https://dce-noroff-api.herokuapp.com/guitars")
        const jsonGuitars = await response.json()
        return jsonGuitars
    }
    catch (error) {
        console.error("Error: ", error.message)
    }
}

// Module Pattern & IIFEs
(async () => {
    // DOM Elements
    const guitarsSelectElement = document.getElementById("guitars")
    const guitarImageElement = document.getElementById("guitar-image")
    const guitarTitleElement = document.getElementById("guitar-title")
    const guitarMaterialsElement = document.getElementById("guitar-materials")

    // Variables
    const guitars = await fetchGuitars()

    for (const guitar of guitars) {
        guitarsSelectElement.innerHTML += `<option value=${guitar.id}>${guitar.model}</option>`
    }

    // Functions
    const renderGuitar = (selectedGuitar) => {
        // guitarImageElement.src = "" // or loading gif, etc
        guitarImageElement.src = selectedGuitar.image
        guitarTitleElement.innerText = `${selectedGuitar.model} by ${selectedGuitar.manufacturer}`

        guitarMaterialsElement.innerHTML = ""
        for (const key in selectedGuitar.materials) {
            const material = selectedGuitar.materials[key] //guitar.material.neck
            guitarMaterialsElement.innerHTML += `<li>${material}</li>`
        }
    }

    function onSelectChange() {
        const guitarId = this.value
        const selectedGuitar = guitars.find(g => g.id === guitarId)
        renderGuitar(selectedGuitar)
    }

    guitarsSelectElement.addEventListener("change", onSelectChange)
})()


// The IIFE is Similar to
function init() {

}
init()