class Animal {
    // props
    #type = ""
    name = ""
    emoji = ""

    // constructor
    constructor(type, name, emoji){
        this.emoji = emoji
        this.name = name
        this.#type = type
    }

    getType() {
        return this.#type
    }

    // methods
    printInfo(){
        console.log(this.name + " is a " + this.type + " and looks like " + this.emoji);
    }
}

const cat = new Animal("cat", "Garfield", "🐱")
cat.printInfo()
console.log(cat.getType());

const dog = new Animal("dog", "Bruno", "🐶")
dog.printInfo()





function AnimalFC(type, name, emoji){
    this.type = type
    this.name = name
    this.emoji = emoji

    printInfo = () => {
        console.log(this.name + " is a " + this.type + " and looks like " + this.emoji);
    }

}

// console.log(AnimalFC);
// console.log(Animal);

const catFC = new AnimalFC("catFC", "Yoda", "🦁")

// Spread Operator with Arrays

// replace slice function with spread operator [...]
const sheep = ["🐑","🐑","🐑"]
const fakeSheep = sheep

const cloneSheep = sheep.slice()
const cloneSheep2 = [...sheep, "🐴"]


console.log("Sheep:", sheep);
console.log("Fake Sheep:", fakeSheep);
console.log("Clone Sheep:", cloneSheep);
console.log("Clone Sheep 2:", cloneSheep2);

fakeSheep.push("🐷")

console.log("Sheep:", sheep);
console.log("Fake Sheep:", fakeSheep);
console.log("Clone Sheep:", cloneSheep);
console.log("Clone Sheep 2:", cloneSheep2);

// Spread Operator with objects

const hero = {
    alias: "The Hulk",
    realName: "Bruce Banner",
    superpower: "Hulk Smash!"
}

console.log(hero);

const hero2 = {
    alignment: "evil",
    ...hero,
    superpower: "Something different!"
}

console.log(hero2);