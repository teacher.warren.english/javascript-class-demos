import { users } from "./__mocks__/users.js";

console.log(users);

const GENDER_MALE = "male"
const GENDER_FEMALE = "female"

// HOF (Higher Order Functions)
// Filter -> return a smaller or empty array
// Map -> return an array with the EXACT same size
// Find -> return an array of 1 or 0 elements

// Filter
// true -> the item will be added to the final array
// false -> the item will be ignored
const males = users.filter(user => user.gender === GENDER_MALE) // "male" is a magic string 😭
const females = users.filter(user => user.gender === GENDER_FEMALE)

console.log("Filter Males:", males);
console.log("Filter Females:", females);

// Map
const telemarketers = users.map(function (user, index, users) {
    // doesn't return a boolean, it returns the newly created object
    // Destructuring:
    const { name: {title, first, last}, email } = user
    const nameDisplay = title + " " + first + " " + last
    // const email = user.email
    return {
        // Object Literals
        name: nameDisplay,
        email,
    }
})

console.log("Map Results:", telemarketers);

// Find
const mrJohn = telemarketers.find((telemarketer) => {
    // Returns the first true return value
    return telemarketer.name === "Mr John Ortega"
})

console.log("Find Result:", mrJohn);