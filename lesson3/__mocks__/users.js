export const users = [
    {
        "gender": "male",
        "name": {
            "title": "Mr",
            "first": "Rishaan",
            "last": "Chatterjee"
        },
        "location": {
            "street": {
                "number": 3031,
                "name": "Naiduthota"
            },
            "city": "Dhule",
            "state": "Sikkim",
            "country": "India",
            "postcode": 83099,
            "coordinates": {
                "latitude": "6.8595",
                "longitude": "-33.6841"
            },
            "timezone": {
                "offset": "-2:00",
                "description": "Mid-Atlantic"
            }
        },
        "email": "rishaan.chatterjee@example.com",
        "login": {
            "uuid": "c36494dc-f354-44b2-bf2e-f6023338aa26",
            "username": "bigtiger982",
            "password": "villa",
            "salt": "0purtUjB",
            "md5": "c74f7df6ef1ebc1781d8939f48aeb7ae",
            "sha1": "f6a9a0518f659652262155bae4594629ae329af3",
            "sha256": "506edccbd40d7ce0f82ef9bd31597e7a24529c7dc2c23e843bd92d46208b17e7"
        },
        "dob": {
            "date": "1966-09-24T23:55:49.347Z",
            "age": 55
        },
        "registered": {
            "date": "2003-05-26T22:01:44.275Z",
            "age": 19
        },
        "phone": "7125661372",
        "cell": "7148372857",
        "id": {
            "name": "UIDAI",
            "value": "083258854041"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/14.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/14.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/14.jpg"
        },
        "nat": "IN"
    },
    {
        "gender": "female",
        "name": {
            "title": "Miss",
            "first": "Yasemin",
            "last": "Günday"
        },
        "location": {
            "street": {
                "number": 2128,
                "name": "Necatibey Cd"
            },
            "city": "Osmaniye",
            "state": "Amasya",
            "country": "Turkey",
            "postcode": 46545,
            "coordinates": {
                "latitude": "42.5390",
                "longitude": "-106.4768"
            },
            "timezone": {
                "offset": "-7:00",
                "description": "Mountain Time (US & Canada)"
            }
        },
        "email": "yasemin.gunday@example.com",
        "login": {
            "uuid": "216d5c4e-8c0d-4539-9d87-172eaf61238f",
            "username": "heavywolf805",
            "password": "ministry",
            "salt": "d7Bt5dh1",
            "md5": "02d3aab481a21e183134e94a8fbe2291",
            "sha1": "2606687e9f9a4e8f43aa0f34843934928a903f85",
            "sha256": "7c8592eb64a4d86b66850de8e9a6107bb476a879c2bfb47c6182eab7579d1293"
        },
        "dob": {
            "date": "1997-11-27T07:22:26.929Z",
            "age": 24
        },
        "registered": {
            "date": "2021-12-25T03:21:41.163Z",
            "age": 0
        },
        "phone": "(295)-629-2166",
        "cell": "(825)-769-0167",
        "id": {
            "name": "",
            "value": null
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/16.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/16.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/16.jpg"
        },
        "nat": "TR"
    },
    {
        "gender": "female",
        "name": {
            "title": "Mrs",
            "first": "Gabrielle",
            "last": "Abraham"
        },
        "location": {
            "street": {
                "number": 1984,
                "name": "Charles St"
            },
            "city": "Waterloo",
            "state": "Alberta",
            "country": "Canada",
            "postcode": "G6E 4P7",
            "coordinates": {
                "latitude": "-80.2538",
                "longitude": "-33.8197"
            },
            "timezone": {
                "offset": "+9:30",
                "description": "Adelaide, Darwin"
            }
        },
        "email": "gabrielle.abraham@example.com",
        "login": {
            "uuid": "0f75e01d-97ec-4ff2-8e89-4f32a9d49b9f",
            "username": "yellowduck197",
            "password": "otis",
            "salt": "bNcg9CAq",
            "md5": "b401b22f81e3d99cbb1a8c1c4f6e1773",
            "sha1": "6e840f30a14dd9f9b291e1fe20f0a42608afad78",
            "sha256": "94b5ede94acb8e41710a3448f20d7c852c96ece6042c87ff346ee27f538c5bc9"
        },
        "dob": {
            "date": "1979-10-06T05:23:24.785Z",
            "age": 42
        },
        "registered": {
            "date": "2010-09-20T06:05:56.700Z",
            "age": 11
        },
        "phone": "Z71 E28-0514",
        "cell": "W40 B92-2602",
        "id": {
            "name": "SIN",
            "value": "520063470"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/79.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/79.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/79.jpg"
        },
        "nat": "CA"
    },
    {
        "gender": "male",
        "name": {
            "title": "Mr",
            "first": "Raghav",
            "last": "Mendonsa"
        },
        "location": {
            "street": {
                "number": 4451,
                "name": "Linking Rd"
            },
            "city": "Bhilai",
            "state": "Uttar Pradesh",
            "country": "India",
            "postcode": 19875,
            "coordinates": {
                "latitude": "-81.9205",
                "longitude": "-142.6958"
            },
            "timezone": {
                "offset": "0:00",
                "description": "Western Europe Time, London, Lisbon, Casablanca"
            }
        },
        "email": "raghav.mendonsa@example.com",
        "login": {
            "uuid": "ac29705e-33e0-4d0a-81f7-64af3196dfc1",
            "username": "lazyduck746",
            "password": "pearson",
            "salt": "R9nUP8Hr",
            "md5": "eb62352f735f539508711eae04aa084b",
            "sha1": "0ed43ba9479b84cd9353b97f84028c054109477b",
            "sha256": "291f86e8cedd8298732638616514c532c91ad6d05bb2998bd0706171e182cf24"
        },
        "dob": {
            "date": "1989-06-28T06:02:31.274Z",
            "age": 33
        },
        "registered": {
            "date": "2015-08-20T18:11:11.834Z",
            "age": 7
        },
        "phone": "7866948013",
        "cell": "7959535682",
        "id": {
            "name": "UIDAI",
            "value": "233171171839"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/88.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/88.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/88.jpg"
        },
        "nat": "IN"
    },
    {
        "gender": "female",
        "name": {
            "title": "Ms",
            "first": "Gunhild",
            "last": "Stensby"
        },
        "location": {
            "street": {
                "number": 8684,
                "name": "Skolestien"
            },
            "city": "Straume",
            "state": "Telemark",
            "country": "Norway",
            "postcode": "2219",
            "coordinates": {
                "latitude": "-60.8448",
                "longitude": "-79.7531"
            },
            "timezone": {
                "offset": "+5:30",
                "description": "Bombay, Calcutta, Madras, New Delhi"
            }
        },
        "email": "gunhild.stensby@example.com",
        "login": {
            "uuid": "d0926d70-1f43-4a13-80ab-714e2216b4aa",
            "username": "tinyladybug147",
            "password": "soleil",
            "salt": "ELHUFxCa",
            "md5": "9d6c82618551c568886788512b20f1c0",
            "sha1": "25bf37d1948a9e808eeec5edda7c47ef6955c732",
            "sha256": "604e5361240578aaf5fcf5e7c59dd7d335d5be31106c9fc0db214bba9473dcf3"
        },
        "dob": {
            "date": "1970-04-22T15:41:30.541Z",
            "age": 52
        },
        "registered": {
            "date": "2012-11-04T15:24:06.248Z",
            "age": 9
        },
        "phone": "31293522",
        "cell": "99548207",
        "id": {
            "name": "FN",
            "value": "22047010636"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/31.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/31.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/31.jpg"
        },
        "nat": "NO"
    },
    {
        "gender": "female",
        "name": {
            "title": "Miss",
            "first": "Julia",
            "last": "Bélanger"
        },
        "location": {
            "street": {
                "number": 3971,
                "name": "Dalhousie Ave"
            },
            "city": "Princeton",
            "state": "Prince Edward Island",
            "country": "Canada",
            "postcode": "H4C 2G3",
            "coordinates": {
                "latitude": "-46.2792",
                "longitude": "99.9990"
            },
            "timezone": {
                "offset": "-6:00",
                "description": "Central Time (US & Canada), Mexico City"
            }
        },
        "email": "julia.belanger@example.com",
        "login": {
            "uuid": "11daf89b-a585-408c-9589-a342a2bb6ca7",
            "username": "goldenkoala182",
            "password": "sausage",
            "salt": "jUh81Ea6",
            "md5": "1916bc51d9760fd47e0f22a9e0de8ae7",
            "sha1": "517ff32689e258be77128579b55e5c984b07871b",
            "sha256": "220c08ade3013f264e92ea794a141544a928f3ce34c7f28ed90ca99a910dfeab"
        },
        "dob": {
            "date": "1980-05-18T14:09:47.351Z",
            "age": 42
        },
        "registered": {
            "date": "2020-04-24T05:04:42.266Z",
            "age": 2
        },
        "phone": "O74 L60-8459",
        "cell": "S83 X68-0390",
        "id": {
            "name": "SIN",
            "value": "805748373"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/28.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/28.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/28.jpg"
        },
        "nat": "CA"
    },
    {
        "gender": "female",
        "name": {
            "title": "Mrs",
            "first": "Victoria",
            "last": "Johnson"
        },
        "location": {
            "street": {
                "number": 5423,
                "name": "Forest Ln"
            },
            "city": "Colorado Springs",
            "state": "Ohio",
            "country": "United States",
            "postcode": 47920,
            "coordinates": {
                "latitude": "63.1038",
                "longitude": "-143.1164"
            },
            "timezone": {
                "offset": "-11:00",
                "description": "Midway Island, Samoa"
            }
        },
        "email": "victoria.johnson@example.com",
        "login": {
            "uuid": "f5ad97ff-77c1-4f86-9b71-b305a3f23718",
            "username": "tinyladybug529",
            "password": "babylon5",
            "salt": "m6dNbyOl",
            "md5": "05641196d4bbe43381509af40473f61d",
            "sha1": "cba3a3187d6b98b8b5df7deee584e30ed49028b9",
            "sha256": "753713eb6dba990d48b57ba67d4a61eb7e22ffe797db8c930ee55447d0ca53d8"
        },
        "dob": {
            "date": "1983-03-29T20:56:02.404Z",
            "age": 39
        },
        "registered": {
            "date": "2006-06-10T14:48:20.736Z",
            "age": 16
        },
        "phone": "(389) 466-5630",
        "cell": "(572) 885-1371",
        "id": {
            "name": "SSN",
            "value": "585-51-8316"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/66.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/66.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/66.jpg"
        },
        "nat": "US"
    },
    {
        "gender": "male",
        "name": {
            "title": "Mr",
            "first": "John",
            "last": "Ortega"
        },
        "location": {
            "street": {
                "number": 9647,
                "name": "Avenida de Andalucía"
            },
            "city": "Arrecife",
            "state": "Cantabria",
            "country": "Spain",
            "postcode": 24043,
            "coordinates": {
                "latitude": "49.4757",
                "longitude": "-66.7897"
            },
            "timezone": {
                "offset": "+5:30",
                "description": "Bombay, Calcutta, Madras, New Delhi"
            }
        },
        "email": "john.ortega@example.com",
        "login": {
            "uuid": "f33f98da-5245-4d09-b635-8cca352789cd",
            "username": "heavyladybug352",
            "password": "strawber",
            "salt": "5bSr3GX7",
            "md5": "56033b345b1dbb0cc3f0641bdccabbad",
            "sha1": "5f80f66dc93de8c64ea00d125901147d4e129ca8",
            "sha256": "00b8d98a0b79401f8c529af80f1df7a63b34218fb1adfdf8b7daceed021a0128"
        },
        "dob": {
            "date": "1988-06-07T00:29:39.498Z",
            "age": 34
        },
        "registered": {
            "date": "2006-05-19T18:07:15.091Z",
            "age": 16
        },
        "phone": "916-935-949",
        "cell": "682-297-386",
        "id": {
            "name": "DNI",
            "value": "06519617-E"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/39.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/39.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/39.jpg"
        },
        "nat": "ES"
    },
    {
        "gender": "female",
        "name": {
            "title": "Miss",
            "first": "Anna",
            "last": "Neal"
        },
        "location": {
            "street": {
                "number": 2828,
                "name": "Mill Lane"
            },
            "city": "Manchester",
            "state": "Fife",
            "country": "United Kingdom",
            "postcode": "EN1 8GJ",
            "coordinates": {
                "latitude": "-12.5317",
                "longitude": "-142.3749"
            },
            "timezone": {
                "offset": "+3:00",
                "description": "Baghdad, Riyadh, Moscow, St. Petersburg"
            }
        },
        "email": "anna.neal@example.com",
        "login": {
            "uuid": "9400a57b-04b3-479b-84b8-7772f1bf3baf",
            "username": "happyduck373",
            "password": "mushroom",
            "salt": "bD7L6EKT",
            "md5": "ffce05e23fc3fdfe804df494c1644b53",
            "sha1": "c48da102a4c149947e2978ee29cc980b87b92c4a",
            "sha256": "0feeffb9f1cc7dd5311855a31a194fb6cf71c1e792aa9f2f863068d907e57b88"
        },
        "dob": {
            "date": "1994-01-04T12:16:48.670Z",
            "age": 28
        },
        "registered": {
            "date": "2008-02-12T13:14:47.570Z",
            "age": 14
        },
        "phone": "015394 54141",
        "cell": "07076 919489",
        "id": {
            "name": "NINO",
            "value": "JY 63 53 13 Z"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/women/58.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/58.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/58.jpg"
        },
        "nat": "GB"
    },
    {
        "gender": "male",
        "name": {
            "title": "Mr",
            "first": "Eelko",
            "last": "Van der Voort"
        },
        "location": {
            "street": {
                "number": 3030,
                "name": "Karawanken"
            },
            "city": "Noordeinde Nh",
            "state": "Drenthe",
            "country": "Netherlands",
            "postcode": "1563 OQ",
            "coordinates": {
                "latitude": "6.8608",
                "longitude": "-78.3591"
            },
            "timezone": {
                "offset": "+10:00",
                "description": "Eastern Australia, Guam, Vladivostok"
            }
        },
        "email": "eelko.vandervoort@example.com",
        "login": {
            "uuid": "a83bcae3-daaf-47d1-a53f-8f848ad44452",
            "username": "greendog813",
            "password": "8888",
            "salt": "Tu0dcHO4",
            "md5": "3ac06c32923cbe653f74c8c6c0cfb2b3",
            "sha1": "d7d487422405112c386016b815a82615dc046c2a",
            "sha256": "de077bfedafcf74ca4b93caa41bdc3a96830c4969591149dd988139051215f7e"
        },
        "dob": {
            "date": "1993-01-26T08:55:59.603Z",
            "age": 29
        },
        "registered": {
            "date": "2017-06-11T16:50:22.193Z",
            "age": 5
        },
        "phone": "(0799) 983196",
        "cell": "(06) 86604703",
        "id": {
            "name": "BSN",
            "value": "87305060"
        },
        "picture": {
            "large": "https://randomuser.me/api/portraits/men/28.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/28.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/28.jpg"
        },
        "nat": "NL"
    }
]