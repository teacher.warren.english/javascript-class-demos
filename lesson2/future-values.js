const userListElement = document.getElementById("user-list")

console.log("Before the fetch");

fetch("https://randomuser.me/api?results=2")
.then(function (response) {
    console.log("Response Data:", response);
    return response.json()
})
.then(function(json) {
    console.log("JSON Data:", json);
    renderJsonData(json.results)
})

.catch(function(error) {
    console.log(error);
})

console.log("After the fetch");

// Functions

function renderJsonData(users){
    console.log("Users:", users);
    for (const user of users){
        userListElement.innerHTML += `<li>${user.name.title} ${user.name.first} ${user.name.last}</li>` // string interpolation $"{}" AKA template literals (with backticks``````)
    }
}