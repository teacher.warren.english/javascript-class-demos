// Demo Todos

function Todo(id, title){
    this.id = id
    this.title = title
    this.completed = false
}

// Variables
const myTodos = [] // out list of todo items
const todo = new Todo(1, "Eat Breakfast") // just a dummy example

// DOM Elements

const todoInputElement = document.getElementById("new-todo")
const todoButtonElement = document.getElementById("add-todo")
const todoListElement = document.getElementById("todo-list") // <ul> -> <li>

todoButtonElement.addEventListener("click", onButtonClick)

// Handlers

function onButtonClick(){
    // Get value from input
    const newTodoTitle = todoInputElement.value.trim()
    
    if (newTodoTitle === ""){
        alert("Empty values are not allowed!")
        todoInputElement.value = ""
        return
    }
    
    console.log(newTodoTitle);

    // Create a new todo item
    const newId = createId()
    const newTodo = new Todo(newId, newTodoTitle)

    // Add todo item to a list
    myTodos.push(newTodo)

    // Render it to the UI
    renderTodosWithForOf(myTodos)
}

// Functions
function createId(){
    return Math.random().toString(16).slice(2)
}

function renderTodosWithFor(todos){
    todoListElement.innerHTML = ""
    for (let i = 0; i < todos.length; i++){
        todoListElement.innerHTML += "<li>" + todos[i].title + "</li>"
    }
}

function renderTodosWithForOf(todos){
    todoListElement.innerHTML = ""
    for (const todo of todos) {
        const newListElement = document.createElement("li")
        newListElement.innerText = todo.title + ": " + todo.completed
        todoListElement.appendChild(newListElement)
    }
}